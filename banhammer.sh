#!/bin/bash

# This function is for word blocking on a specific port
ban_word() {
    local port word
    port=$1
    word=$2

    iptables -I INPUT -p tcp --dport "$port" -m string --algo bm --string "$word" -j DROP
    echo "The word '$word' is blocked on the '$port' port."
}

# This function is for displaying a list of blocked words on various ports
list_bans() {
    echo "| № | port | word    |"
    echo "|---|------|---------|"
    local rule_number=0
    iptables -L INPUT -n --line-numbers | grep match | while read -r line; do
        ((rule_number++))
        port=$(echo "$line" | awk '{print $8}'| awk -F ":" '{print $2}')
        word=$(echo "$line" | awk '{print $11}')
        echo "| $rule_number | $port | $word |"
    done
}

# This function is for unlocking words on a specific port
unban_word() {
    local port word rule_number
    if [[ $2 == "-n" ]]; then
        rule_number=$3
        iptables -D INPUT "$rule_number"
        echo "The lock with rule №$rule_number has been removed."
    elif [[ $2 == "--port" && $4 == "--word" ]]; then
        port=$3
        word=$5
        rule_number=$(iptables -L INPUT -n --line-numbers | grep "$port" | grep "$word" | awk '{print $1}')
        if [[ -n $rule_number ]]; then
            iptables -D INPUT "$rule_number"
            echo "Blocking the word '$word' on the port '$port' has been deleted."
        else
            echo "Blocking the word '$word' was not found on the port '$port'."
        fi
    else
        echo "Incorrect format of the unlock command."
        exit 1
    fi
}

# Parsing arguments
if [[ $1 == "ban" ]]; then
    shift
    while [[ "$#" -gt 0 ]]; do
        case $1 in
        --port)
            port="$2"
            shift
            ;;
        --word)
            word="$2"
            shift
            ;;
        *)
            echo "Unknown argument: $1"
            exit 1
            ;;
        esac
        shift
    done

    ban_word "$port" "$word"

elif [[ $1 == "list" ]]; then
    list_bans

elif [[ $1 == "unban" ]]; then
    unban_word "$@"
else
    echo "You can use: $0 {ban --port <port> --word <word> | list | unban -n <number> | unban --port <port> --word <word>}"
    exit 1
fi

